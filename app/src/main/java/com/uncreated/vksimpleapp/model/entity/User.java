package com.uncreated.vksimpleapp.model.entity;

public class User {
    private String id;
    private String firstName;
    private String lastName;
    private String photo;

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoto() {
        return photo;
    }
}
